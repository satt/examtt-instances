import json



class Solution(object):
    '''Class representing a problem solution'''
    
    formats = ['sol']
    '''List of available formats (str)'''
    
    default_format = 'sol'
    '''Default format (str)'''
    
    def __init__(self, sol_content, sol_format = None):
        '''Loads a solution object from the solution content as string'''
		
        if sol_format not in self.formats:
            raise ValueError('Unsupported solution format: ' + str(sol_format) + '. Try one of {' + ','.join(map(str,self.formats)) + '} instead.')        
        
        self.result = []
        
		# read solution row by row
        for row in sol_content.split('\n'):
            
            # skip empty rows
            if row.strip() == '':
                continue
            
            c = map(lambda x: x.strip(), row.strip().split(','))
			
            if len(c) == 2:
                self.result.append({'period':int(c[0]), 'room':int(c[1])})
            	


    def __str__(self):
        '''Returns a string representation of the solution (optional, for debug)'''
        
        return json.dumps({ })
		
