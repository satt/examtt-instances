import json

class Instance(object):
	'''Class representing a problem instance'''
    
	formats = ['ITC']
	'''List of available formats (str)'''
    
	default_format = 'ITC'
	'''Default format (str)'''
    
	def __init__(self, inst_content, inst_format = None):
		'''Loads an instance object from the instance content as string'''
        
		if inst_format not in self.formats:
			raise ValueError('Unsupported instance format: ' + str(inst_format) + '. Try one of {' + ','.join(map(str,self.formats)) + '} instead.')        
        
		# initialize fields
		self.imm = []
		self.durations = []
		self.rooms = []
		self.meta = {}
		self.periods = []
		self.period_costraint = []
		self.room_costraint = []
		self.InstWeight = {}
		
		# read input row by row
		row = inst_content.split('\n')
        
		
		# Exams Section
		c = row[0].split(':')
		n = c[1].split(']')
		self.meta['Exams'] = int(n[0])
		
		for i in range(1,self.meta['Exams']+1):
			
			e = map(int, row[i].split(','))
			self.durations.append(e.pop(0))
			s = set(e)
			if len(s) < len(e):
				raise ValueError('Exam ' + str(i-1) + ' has students registered multiple times') 
#			if len(s)==0:	
#				raise ValueError('Exam ' + str(i-1) + ' has no students')      
			self.imm.append(s)
			
		self.N_Students = []
		for e in self.imm:
			self.N_Students.append(len(e))
			
		del row[0:self.meta['Exams']+1]
		
		# Periods Section
		c = row[0].split(':')
		n = c[1].split(']')
		self.meta['Periods'] = int(n[0])
		
		max_periods=0
		for i in range(1,self.meta['Periods']+1):
			p = row[i].split(',')
			dur = int(p[2])
			self.periods.append({'date':p[0],'duration':dur,'penalty':int(p[3])})
			if(dur>max_periods):
				max_periods=dur		
		
		if max(self.durations) > max_periods:
			raise ValueError('The longest exam (Duration: ' + str(max(self.durations)) + ') is longer then the longest period (Duration: ' + str(max_periods) + ')')
		
		del row[0:self.meta['Periods']+1]
		
		# Rooms Section
		c = row[0].split(':')
		n = c[1].split(']')
		self.meta['Rooms'] = int(n[0])
		
		max_rooms=0
		for i in range(1,self.meta['Rooms']+1):
			r = row[i].split(',')
			cap = int(r[0])
			self.rooms.append({'capacity':cap,'penalty':int(r[1])})
			if(cap>max_rooms):
				max_rooms=cap
				
		if max(self.N_Students) > max_rooms:
			raise ValueError('The biggest room (Capacity: ' + str(max_rooms) + ') cant contain the exam with the highest number of students registered (Students: ' + str(max(self.N_Students)) + ')')
		
		
		del row[0:self.meta['Rooms']+1]
		
		for r in row:
		
			c = map(lambda x: x.strip(), r.strip().split(','))
			
			if  r.strip() == '':
				continue
				
			elif len(c) == 2:
				if c[1]=='ROOM_EXCLUSIVE':
					self.room_costraint.append(int(c[0]))
					if int(c[0]) >= self.meta['Exams']:
						raise ValueError('Exams ' + str(c[0]) + ' doesnt exist')      
				else:
					self.InstWeight[c[0]] = int(c[1])
					if int(c[1]) < 0:
						raise ValueError(c[0] + ' has a negative value')     	
							
			elif len(c) == 3:
				self.period_costraint.append({'relation': c[1], 'e_pair': [int(c[0]),int(c[2])]})
				if int(c[0]) >= self.meta['Exams']:
					raise ValueError('Exams ' + str(int(c[0])) + ' doesnt exist')
				if int(c[2]) >= self.meta['Exams']:
					raise ValueError('Exams ' + str(int(c[2])) + ' doesnt exist')	
						
			elif len(c) == 4:
				self.InstWeight[c[0]] = [int(c[1]),int(c[2]),int(c[3])]
				if int(c[1]) >= self.meta['Exams']:
					raise ValueError('Too many exams for the Frontload Constraint')
				if int(c[2]) >= self.meta['Periods']:
					raise ValueError('Too many periods for the Frontload Constraint')
				if int(c[3]) < 0:
					raise ValueError(c[0] + ' has a negative penalty')
		
		count = 0    
		self.StudentInCommon = [ self.meta['Exams']*[0] for p in range(self.meta['Exams']) ]
		for e1 in range(self.meta['Exams']-1):
			for e2 in range(e1+1,self.meta['Exams']):
				self.StudentInCommon[e1][e2]=self.StudentInCommon[e2][e1]=len(self.imm[e1].intersection(self.imm[e2]))
				if self.StudentInCommon[e1][e2] > 0:
					count += 1
		
		Conflict_Exams = 2*count/float(self.meta['Exams'])

		self.meta['Conflict Density'] = (Conflict_Exams/float(self.meta['Exams']))
			
		self.students = set([])
		for e in self.imm:
			self.students= set(self.students.union(e))
		self.meta['Students'] = len(self.students)
        
        
		if  not self.InstWeight.has_key('PERIODSPREAD') or  not self.InstWeight.has_key('TWOINAROW') or  not self.InstWeight.has_key('TWOINADAY') or  not self.InstWeight.has_key('NONMIXEDDURATIONS') or  not self.InstWeight.has_key('FRONTLOAD'):
			raise ValueError('Error in reading the Institutional Weightings')
		
		if self.InstWeight['PERIODSPREAD']>self.meta['Periods']:
			raise ValueError('Period Spread longer then the number of periods')
			
			
	def __str__(self):
		'''Returns a string representation of the instance (optional, for debug)'''
        
		return json.dumps({ })
		
		
