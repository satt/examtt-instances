#!/usr/bin/env python
import argparse
import json

# if validator is used standalone, import symbols from instance_loader module
if __name__ == '__main__':
    from instance_loader import *

class InstanceValidator(object):
    '''Instance validation logic'''

    @classmethod
    def validate(cls, inst_content, inst_format = None):
        '''Takes an instance as input string, evaluates its validity, computes features'''

        # correctly handle None format
        if not inst_format:
            inst_format = Instance.default_format

        # initialize feature map
        features = { }

        # compute features
        try:
            i = Instance(inst_content, inst_format)

            # compute features here, raise exception on failure
            features.update(i.meta)
            features["Penalty TwoInARow"] = i.InstWeight["TWOINAROW"]
            features["Penalty TwoInADay"] = i.InstWeight["TWOINADAY"]
            features["Period Spread"]=i.InstWeight["PERIODSPREAD"]
            features["Frontload: Exams"]=i.InstWeight["FRONTLOAD"][0]
            features["Frontload: Periods"]=i.InstWeight["FRONTLOAD"][1]
            features["Frontload: Penalty"]=i.InstWeight["FRONTLOAD"][2]
            features["Penalty for Mixed Durations"]=i.InstWeight["NONMIXEDDURATIONS"]
            features["Period Constraints"] = len(i.period_costraint)
            features["Rooms Constraints"] = len(i.room_costraint)
			# ...

        # print exception as JSON (compliant with validator logic)
        except Exception as e:
            return { 'valid': False, 'reason': str(e) }

        return {
            'valid': True,
            'format': inst_format,
            'features': features
        }

def main():
    '''Entry point for when the validator is called from the command line, returns valid JSON'''

    # setup CLI
    parser = argparse.ArgumentParser(description='Instance validator')
    parser.add_argument('--inst-file', '-i', required = True, type = str, help='instance file')
    parser.add_argument('--inst-format', '-f', required = False, type = str, choices = Instance.formats, default=Instance.default_format, help='instance format name')
    args = parser.parse_args()

    # open instance file
    f = open(args.inst_file)
    inst_content = f.read()
    f.close()

    # activate validator, print result
    result = InstanceValidator.validate(inst_content, args.inst_format)
    print json.dumps(result, indent=4)

# when invoked from the command line, call main()
if __name__ == '__main__':
    main()
