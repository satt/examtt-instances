#!/usr/bin/env python
import argparse
import json
from math import fabs


# if validator is used standalone, import symbols from instance/solution_loader modules
if __name__ == '__main__':
    from instance_loader import *
    from solution_loader import *

class SolutionValidator(object):
	'''Solution validation logic'''

	@classmethod
	def validate(cls, inst_content, inst_format, sol_content, sol_format):
		'''Takes a solution and an instance input as string, evaluates its validity, computes features.'''

		# correctly handle None format
		if not inst_format:
			inst_format = Instance.default_format

		# correctly handle None format
		if not sol_format:
			sol_format = Solution.default_format

		# initialize cost components
		cost_components = { }
		cost = None
		
		# compute costs
		try:
			s = Solution(sol_content, sol_format)
			i = Instance(inst_content, inst_format)
			
			for e in s.result:
				if e['period'] >= i.meta['Periods']:
					raise ValueError('Period ' + str(e['period']) + ' doesnt exist')
				if e['room'] >= i.meta['Rooms']:
					raise ValueError('Room ' + str(e['room']) + ' doesnt exist')
			
			if len(s.result)!=i.meta["Exams"]:
				raise ValueError('Wrong number of exams in the solution')
			
			
			# compute cost components here, raise exception on failure
			cost_components['Room Constraint (hard)']= 0
			cost_components['Periods Penalty (soft)'] = 0
			cost_components['Rooms Penalty (soft)'] = 0
			cost_components['Excessive Duration (hard)'] = 0
			cost_components['Period Constraint (hard)'] = 0
			cost_components['Two Exams In A Row (soft)'] = 0
			cost_components['Two Exams In A Day (soft)'] = 0
			cost_components['Period Spread (soft)'] = 0
			cost_components['Frontload (soft)'] = 0
			cost_components['Simultaneous Exams (hard)'] = 0
			cost_components['Overcapacity (hard)'] = 0
			cost_components['Mixed Duration (soft)'] = 0


			
			for c in i.period_costraint:
			
				if c['relation']== 'AFTER':
					if s.result[c['e_pair'][0]]['period'] <= s.result[c['e_pair'][1]]['period']:
						cost_components['Period Constraint (hard)']+=1

				elif c['relation']== 'EXCLUSION':
					if s.result[c['e_pair'][0]]['period'] == s.result[c['e_pair'][1]]['period']:
						cost_components['Period Constraint (hard)']+=1

				elif c['relation']== 'EXAM_COINCIDENCE':
					if s.result[c['e_pair'][0]]['period'] != s.result[c['e_pair'][1]]['period']:
						cost_components['Period Constraint (hard)']+=1

			
			for e1 in range(i.meta['Exams']-1):
				for e2 in range(e1+1,i.meta['Exams']):
				
					if(s.result[e1]['period'] == s.result[e2]['period']):
						cost_components['Simultaneous Exams (hard)'] += i.StudentInCommon[e1][e2]
					else:
						if(i.periods[s.result[e1]['period']]['date'] == i.periods[s.result[e2]['period']]['date']):
							if (fabs(s.result[e1]['period'] - s.result[e2]['period'])== 1):
								cost_components['Two Exams In A Row (soft)'] += i.StudentInCommon[e1][e2]*i.InstWeight['TWOINAROW']
							else:
								cost_components['Two Exams In A Day (soft)'] += i.StudentInCommon[e1][e2]*i.InstWeight['TWOINADAY']
					
						if(fabs(s.result[e1]['period'] - s.result[e2]['period'])<=i.InstWeight['PERIODSPREAD']):
							cost_components['Period Spread (soft)'] += i.StudentInCommon[e1][e2]
			

			#frontload
			aux = range(i.meta['Exams'])
			FL = []
			max = 0
			for f in range(i.InstWeight['FRONTLOAD'][0]):
				max = len(i.imm[aux[0]])
				index=0
				for j in range(1,len(aux)):
					if  len(i.imm[aux[j]])> max:
						index = j
						max = len(i.imm[aux[j]])
				FL.append(aux[index])
				del aux[index]
			
			for f in FL:
				if(s.result[f]['period']>= (i.meta['Periods']-i.InstWeight['FRONTLOAD'][1])):
					cost_components['Frontload (soft)'] += i.InstWeight['FRONTLOAD'][2]

			
			StudentInRoom = [ i.meta['Rooms']*[0] for p in range(i.meta['Periods']) ]
			Mixed = [ i.meta['Rooms']*[[]] for p in range(i.meta['Periods']) ]

			
			for e1 in range(i.meta['Exams']):				

				StudentInRoom[s.result[e1]['period']][s.result[e1]['room']] += len(i.imm[e1])

				if(i.durations[e1] not in Mixed[s.result[e1]['period']][s.result[e1]['room']]):
					aux = list(Mixed[s.result[e1]['period']][s.result[e1]['room']])
					aux.append(i.durations[e1])
					Mixed[s.result[e1]['period']][s.result[e1]['room']] = list(aux)

				cost_components['Periods Penalty (soft)'] += i.periods[s.result[e1]['period']]['penalty']
				cost_components['Rooms Penalty (soft)'] += i.rooms[s.result[e1]['room']]['penalty']

				if (i.durations[e1] > i.periods[s.result[e1]['period']]['duration']):
					cost_components['Excessive Duration (hard)']+=1
				
			for p in range(i.meta['Periods']):
				for r in range(i.meta['Rooms']):
					if StudentInRoom[p][r] > i.rooms[r]['capacity']:
						cost_components['Overcapacity (hard)']+= StudentInRoom[p][r] - i.rooms[r]['capacity']
					if len(Mixed[p][r])>1:
						cost_components['Mixed Duration (soft)']+= ((len(Mixed[p][r])-1)*i.InstWeight['NONMIXEDDURATIONS'])
			
			for e in i.room_costraint:
				if s.result.count(s.result[e])>1:
					print e
					cost_components['Room Constraint (hard)'] += 1
			
			soft_cost = cost_components['Two Exams In A Row (soft)'] + cost_components['Period Spread (soft)'] + cost_components['Two Exams In A Day (soft)']+cost_components['Periods Penalty (soft)']+cost_components['Rooms Penalty (soft)'] +cost_components['Frontload (soft)'] + cost_components['Mixed Duration (soft)']
			hard_cost = cost_components['Simultaneous Exams (hard)'] + cost_components['Period Constraint (hard)'] + cost_components['Room Constraint (hard)']+cost_components['Overcapacity (hard)']+cost_components['Excessive Duration (hard)']

			cost = soft_cost + 1000*hard_cost


		# print exception as JSON (compliant with validator logic)
		except Exception as e:
			return { 'valid': False, 'reason': str(e) }
		return {
			'valid': True,
			'format': sol_format,
			'cost_components': cost_components,
			'cost': cost
		}

def main():
    '''Entry point for when the solution validator is called from the command line, returns valid JSON.'''

    # setup CLI
    parser = argparse.ArgumentParser(description='Solution validator')
    parser.add_argument('--inst-file', '-i', required = True, type = str, help='instance file')
    parser.add_argument('--sol-file', '-s', required = True, type = str, help='solution file')
    parser.add_argument('--inst-format', '-if', required = False, default=Instance.default_format, choices=Instance.formats, type = str, help='instance format name')
    parser.add_argument('--sol-format', '-sf', required = False, default=Solution.default_format, choices=Solution.formats, type = str, help='solution format name')
    args = parser.parse_args()

    # open instance file
    f = open(args.inst_file)
    inst_content = f.read()
    f.close()

    # open solution file
    f = open(args.sol_file)
    sol_content = f.read()
    f.close()

    # activate validator, print result
    result = SolutionValidator.validate(inst_content, args.inst_format, sol_content, args.sol_format)
    print json.dumps(result, indent=4)


# when invoked from the command line, call main()
if __name__ == '__main__':
    main()
