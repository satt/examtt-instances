# Examination timetabling instances


This repository contains the artificial instances of the Examination Timetabling problem defined in [ITC2007](http://www.cs.qub.ac.uk/itc2007/), used in the paper *[Feature-based tuning of simulated annealing for examination timetabling](http://dx.doi.org/10.1007/s10479-015-2061-8)* by Michele Battistutta, Andrea Schaerf, and Tommaso Urli, Annals of Operations Research, 252(2):239--254, 2017. 

We provide also the results of our experiments on both the real
instances and the artificial ones. Finally, we provide a validation
software for the problem.

## Instances

Instances are stored in the folder [Instances](https://bitbucket.org/satt/examtt-instances/src). 

## Results

The spreadsheets containing the costs of the solutions on our solver are stored in the folder [Results](https://bitbucket.org/satt/examtt-instances/src).

## Validators

These tools give the possibility to validate new instances and new solutions.
They are available in the folder [Validation](https://bitbucket.org/satt/examtt-instances/src). Python needs to be installed to run the validators.

To validate an instance and compute its features, run

        instance_validator.py --inst-file <INST_FILE_NAME>

To validate a solution and report its costs, run 

        solution_validator.py --inst-file <INST_FILE_NAME> --sol-file <SOL_FILE_NAME>



